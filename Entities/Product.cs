﻿using System;
using System.Collections.Generic;

namespace Richa_Milestone2CodingTest_9Sep3PM.Entities
{
    public partial class Product
    {
        public Product()
        {
            ProductOrder = new HashSet<ProductOrder>();
        }

        public int Productid { get; set; }
        public string Productname { get; set; }
        public string Description { get; set; }
        public decimal? Amount { get; set; }
        public int? Stock { get; set; }
        public int? Catid { get; set; }
        public string Photo { get; set; }

        public virtual Categories Cat { get; set; }
        public virtual ICollection<ProductOrder> ProductOrder { get; set; }
    }
}
