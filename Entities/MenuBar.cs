﻿using System;
using System.Collections.Generic;

namespace Richa_Milestone2CodingTest_9Sep3PM.Entities
{
    public partial class MenuBar
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public bool? Openinnewwindow { get; set; }
    }
}
