﻿using System;
using System.Collections.Generic;

namespace Richa_Milestone2CodingTest_9Sep3PM.Entities
{
    public partial class ApplicationUser
    {
        public ApplicationUser()
        {
            ProductOrder = new HashSet<ProductOrder>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public long? Phone { get; set; }
        public int? Pincode { get; set; }
        public string House { get; set; }
        public string Road { get; set; }
        public string City { get; set; }
        public string State { get; set; }

        public virtual ICollection<ProductOrder> ProductOrder { get; set; }
    }
}
