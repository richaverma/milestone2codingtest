﻿using System;
using System.Collections.Generic;

namespace Richa_Milestone2CodingTest_9Sep3PM.Entities
{
    public partial class Categories
    {
        public Categories()
        {
            Product = new HashSet<Product>();
        }

        public int Catid { get; set; }
        public string Catname { get; set; }

        public virtual ICollection<Product> Product { get; set; }
    }
}
