﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Richa_Milestone2CodingTest_9Sep3PM.Entities;
using Richa_Milestone2CodingTest_9Sep3PM.Features.Queries;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Richa_Milestone2CodingTest_9Sep3PM.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
       private readonly ISender _sender;
        private GroceryDbContext _context;

        public HomeController(ISender sender)
        {
            _sender = sender;
        }



        [HttpGet]
        public async Task<IEnumerable<Product>> ViewAllProducts()
        {
            return await _sender.Send(new GetProductsQuery());
        }



        [HttpGet]
        public async Task<IEnumerable<Categories>> ViewIEnumerableOfCategories()
        {
            return await _sender.Send(new GetCategoriesQuery());
        }



        [HttpGet]
        public async Task<Product> GetProductDetailsById(int productid)
        {
            return await _sender.Send(new GetProductByIdQuery() { ProductId = productid });
        }



        [HttpGet]
        public async Task<IEnumerable<ProductOrder>> ShowOrderInfo(int userid)
        {
            return await _sender.Send(new GetOrdersQuery() { UserId = userid });
        }

        [HttpPost]
        public ProductOrder AddProductOrder(int productid, int userid)
        {
            var orderid = _context.ProductOrder.Max(x => x.Orderid) + 1;
            var product = _context.Product.SingleOrDefault(x => x.Productid == productid);
            var user = _context.ApplicationUser.SingleOrDefault(x => x.Id == userid);
            var productorder = new ProductOrder()
            {
                Userid = userid,
                Productid = productid,
                Orderid = orderid,
                Product = product,
                User = user
            };
            _context.ProductOrder.Add(productorder);
            _context.SaveChanges();
            return productorder;
        }


    }
}
