﻿using Richa_Milestone2CodingTest_9Sep3PM.Entities;
using Richa_Milestone2CodingTest_9Sep3PM.Persistence;
using System.Collections.Generic;
using System.Linq;

namespace Richa_Milestone2CodingTest_9Sep3PM.Repository
{
    public class GroceryServices : IGroceryServices
    {
        /* private readonly GroceryDbContext _context;
         public GroceryServices(GroceryDbContext context)
         {
             _context = context;
         }
         public IEnumerable<Categories> GetCategories()
         {
             return _context.Categories.ToList();
         }

         public IEnumerable<ProductOrder> GetOrderInfo(int UserId)
         {
             return _context.ProductOrder.Where(x => x.Userid == UserId);
         }

         public Product GetProductById(int ProductId)
         {
             return _context.Product.SingleOrDefault(x => x.Productid == ProductId);
         }

         public IEnumerable<Product> GetProducts()
         {
             return _context.Product.ToList();
         }

         public ProductOrder PlaceOrder(int UserId, int ProductId)
         {m
             var orderid = _context.ProductOrder.Max(x => x.Orderid) + 1;
             var product = _context.Product.SingleOrDefault(x => x.Productid == ProductId);
             var user = _context.ApplicationUser.SingleOrDefault(x => x.Id == UserId);
             var productorder = new ProductOrder()
             {
                 Userid = UserId,
                 Productid = ProductId,
                 Orderid = orderid,
                 Product = product,
                 User = user
             };
             _context.ProductOrder.Add(productorder);
             _context.SaveChanges();
             return productorder;
         }
     }*/
        private  GroceryDbContext _context;

        public GroceryServices(GroceryDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Categories> GetCategories()
        {
            return _context.Categories.ToList();
        }

        public IEnumerable<ProductOrder> GetOrderInfo(int UserId)
        {
            return _context.ProductOrder.Where(x => x.Userid == UserId);
        }

        public Product GetProductById(int ProductId)
        {
            return _context.Product.SingleOrDefault(x => x.Productid == ProductId);
        }

        public IEnumerable<Product> GetProducts()
        {
            return _context.Product.ToList();
        }

        public ProductOrder PlaceOrder(int UserId, int ProductId)
        {
            var orderid = _context.ProductOrder.Max(x => x.Orderid) + 1;
            var product = _context.Product.SingleOrDefault(x => x.Productid == ProductId);
            var user = _context.ApplicationUser.SingleOrDefault(x => x.Id == UserId);
            var productorder = new ProductOrder()
            {
                Userid = UserId,
                Productid = ProductId,
                Orderid = orderid,
                Product = product,
                User = user
            };
            _context.ProductOrder.Add(productorder);
            _context.SaveChanges();
            return productorder;
        }
    }
}
