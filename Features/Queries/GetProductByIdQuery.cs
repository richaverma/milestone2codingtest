﻿using MediatR;
using Richa_Milestone2CodingTest_9Sep3PM.Entities;

namespace Richa_Milestone2CodingTest_9Sep3PM.Features.Queries
{
    public class GetProductByIdQuery :IRequest<Product>
    {
       public int ProductId { get; set; }
    }

}
