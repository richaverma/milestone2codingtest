﻿using MediatR;
using Richa_Milestone2CodingTest_9Sep3PM.Entities;
using System.Collections.Generic;

namespace Richa_Milestone2CodingTest_9Sep3PM.Features.Queries
{
    public class GetOrdersQuery :IRequest<IEnumerable<ProductOrder>>
    {
        public int UserId { get; set; }
    }
}
