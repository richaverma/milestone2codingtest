﻿using MediatR;
using Richa_Milestone2CodingTest_9Sep3PM.Entities;
using System.Collections;
using System.Collections.Generic;

namespace Richa_Milestone2CodingTest_9Sep3PM.Features.Queries
{
    public class GetCategoriesQuery:IRequest<IEnumerable<Categories>>
    {
    }
}
