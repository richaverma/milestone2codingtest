﻿using MediatR;
using Richa_Milestone2CodingTest_9Sep3PM.Entities;

namespace Richa_Milestone2CodingTest_9Sep3PM.Features.Commands
{
    public class PlaceOrderCommand :IRequest<ProductOrder>
    {
        public int ProductId { get; set; }
        public int UserId { get; set; }
    }
}
