﻿using MediatR;
using Richa_Milestone2CodingTest_9Sep3PM.Entities;
using Richa_Milestone2CodingTest_9Sep3PM.Features.Queries;
using Richa_Milestone2CodingTest_9Sep3PM.Persistence;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Richa_Milestone2CodingTest_9Sep3PM.Features.Handlers
{
   public class GetProductsQueryHandler : IRequestHandler<GetProductsQuery, IEnumerable<Product>>
    {
        private readonly IGroceryServices GroceryServices;

        public GetProductsQueryHandler(IGroceryServices groceryServices)
        {
            GroceryServices = groceryServices;
        }

        public async Task<IEnumerable<Product>> Handle(GetProductsQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(GroceryServices.GetProducts());
        }
    }
}
