﻿using MediatR;
using Richa_Milestone2CodingTest_9Sep3PM.Entities;
using Richa_Milestone2CodingTest_9Sep3PM.Features.Queries;
using Richa_Milestone2CodingTest_9Sep3PM.Persistence;
using System.Threading;
using System.Threading.Tasks;

namespace Richa_Milestone2CodingTest_9Sep3PM.Features.Handlers
{
    public class GetProductByIdQueryHandler : IRequestHandler<GetProductByIdQuery, Product>
    {
        private readonly IGroceryServices groceryServices;

        public GetProductByIdQueryHandler(IGroceryServices groceryServices)
        {
            this.groceryServices = groceryServices;
        }

        public async Task<Product> Handle(GetProductByIdQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(groceryServices.GetProductById(request.ProductId));
        }
    }
}
