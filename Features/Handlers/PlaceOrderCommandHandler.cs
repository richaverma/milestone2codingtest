﻿using MediatR;
using Richa_Milestone2CodingTest_9Sep3PM.Entities;
using Richa_Milestone2CodingTest_9Sep3PM.Features.Commands;
using Richa_Milestone2CodingTest_9Sep3PM.Persistence;
using Richa_Milestone2CodingTest_9Sep3PM.Repository;
using System.Threading;
using System.Threading.Tasks;

namespace Richa_Milestone2CodingTest_9Sep3PM.Features.Handlers
{
    public class PlaceOrderCommandHandler : IRequestHandler<PlaceOrderCommand, ProductOrder>
    {
        private readonly IGroceryServices groceryServices;

        public PlaceOrderCommandHandler(IGroceryServices groceryServices)
        {
            this.groceryServices = groceryServices;
        }

        public async Task<ProductOrder> Handle(PlaceOrderCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(groceryServices.PlaceOrder(request.UserId ,request.ProductId));
        }
    }
}
