﻿using Richa_Milestone2CodingTest_9Sep3PM.Entities;
using System.Collections.Generic;

namespace Richa_Milestone2CodingTest_9Sep3PM.Persistence
{
    public interface IGroceryServices
    {
       public IEnumerable<Product> GetProducts();
        public IEnumerable<Categories> GetCategories();
        public ProductOrder PlaceOrder(int UserId, int ProductId);
        public Product GetProductById(int ProductId);
        public IEnumerable<ProductOrder> GetOrderInfo(int UserId);
    
        }
       
}
